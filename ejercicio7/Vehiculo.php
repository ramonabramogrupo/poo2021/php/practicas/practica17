<?php

class Vehiculo {

    // propiedades
    public $matricula;
    private $color;
    protected $encendido;
    public $numero;

    
    /**
     * Este metodo se ejecuta automaticamente cuando instancias el objeto
     * Es un metodo magico de php
     */
    public function __construct() {
        $this->numero=func_num_args();  // me indica el numero de argumentos pasados al constructor
    }

    public function encender() {
        $this->encendido = true;
        echo 'Vehiculo encendido <br />';
    }

    public function apagar() {
        $this->encendido = false;
        echo 'Vehiculo apagado <br />';
    }

}


