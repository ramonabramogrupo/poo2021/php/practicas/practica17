<?php

class Vehiculo {

    public $matricula;
    private $color;
    protected $encendido;
    
    /**
     * 
     * Este metodo se ejecuta automaticamente cuando instancias el objeto
     * Es un metodo magico de php
     * @param type $_matricula
     * @param type $_color
     * @param type $_encendido
     */
    public function __construct($_matricula="", $_color="blanco", $_encendido=false) {
        $this->matricula=$_matricula;
        $this->color=$_color;
        $this->encendido=$_encendido;

    }

    public function encender() {
        $this->encendido = true;
        echo 'Vehiculo encendido <br />';
    }

    public function apagar() {
        $this->encendido = false;
        echo 'Vehiculo apagado <br />';
    }

}



