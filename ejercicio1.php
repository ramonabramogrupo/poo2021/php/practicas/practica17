<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        
        /**
         * Clase de tipo usuario
         * Clse para probar conocimientos de metodos y propiedades
         */
        class Usuario{
           // definiendo miembros
           
           //propiedades
           public $nombre="defecto"; // propiedad publica con valor predeterminado y de tipo String
           
           // metodos
           public function setNombre($_nombre="Ramon"){ //metodo publico (setter) nos permite escribir un valor en la propiedad
                $this->nombre=$_nombre;
            }
            
            public function getNombre(){ //metodo publico (getter) nos permite leer el valor de la propiedad nombre
                return $this->nombre;
            }
        }
        
        
        /**
         * Creando un objeto de tipo Usuario
         * Utilizando directamente la propiedad nombre
         */
        
        $persona=new Usuario();  // persona es un objeto de tipo Usuario
        echo $persona->nombre; // leyendo la propiedad nombre de persona (valor es defecto y es de tipo String)
        $persona->nombre="Silvia"; // escribiendo  en la propiedad nombre el valor Silvia
        var_dump($persona);
        
        /**
         * Crear otro objeto de tipo Usuario
         * Utilizar los getter y los setter para acceder a la propiedad nombre
         */
        
        // voy a crear otro usuario
        $persona1=new Usuario();
        echo $persona1->nombre; // leo el valor por defecto de nombre que es "defecto"
        $persona1->setNombre(); // asigno el valor "Ramon" a nombre
        echo $persona1->nombre; // leo el valor de nombre (Ramon)
        
        ?>
    </body>
</html>
